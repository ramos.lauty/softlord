$(document).ready(function() {

        $(".carousel").carousel();

// Contact Form
        $('#contact-form').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
               url: location.origin + "/send",
                data: $(this).serialize()
            });
        });

// Footer Form
        $('#contact-footer').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: location.origin + "/send",
                data: $(this).serialize()
            });

            return false;
        });
});

