<?php
// Routes

$app->get('/', function ($request, $response, $args) {
    return $this->view->render($response, 'home.twig', ['menuActive' => 'inicio']);
});

$app->get('/softlord', function ($request, $response, $args) {
    return $this->view->render($response, 'softlord.twig', ['menuActive' => 'softlord']);
});

$app->get('/pymes', function ($request, $response, $args) {
    return $this->view->render($response, 'pymes.twig', ['menuActive' => 'pymes']);
});

$app->get('/pymesplus', function ($request, $response, $args) {
    return $this->view->render($response, 'pymesplus.twig', ['menuActive' => 'pymesplus']);
});

$app->get('/financieras', function ($request, $response, $args) {
    return $this->view->render($response, 'financieras.twig', ['menuActive' => 'financieras']);
});

$app->get('/contacto', function ($request, $response, $args) {
    return $this->view->render($response, 'contacto.twig', [ 'contactView' => true, 'menuActive' => 'contacto' ]);
});

$app->get('/pymes/bm', function ($request, $response, $args) {
    return $this->view->render($response, 'pymesbm.twig', ['bodyClass' => 'pymes_bm', 'menuActive' => 'pymes']);
});

$app->get('/pymes/cam', function ($request, $response, $args) {
    return $this->view->render($response, 'pymescam.twig', ['bodyClass' => 'pymes_cam', 'menuActive' => 'pymes']);
});

$app->get('/pymes/bam', function ($request, $response, $args) {
    return $this->view->render($response, 'pymesbam.twig', ['bodyClass' => 'pymes_bam', 'menuActive' => 'pymes']);
});

$app->get('/pymesplus/bm', function ($request, $response, $args) {
    return $this->view->render($response, 'pymesplusbm.twig', ['bodyClass' => 'pymesplus_bm', 'menuActive' => 'pymesplus']);
});

$app->get('/pymesplus/cm', function ($request, $response, $args) {
    return $this->view->render($response, 'pymespluscm.twig', ['bodyClass' => 'pymesplus_cm', 'menuActive' => 'pymesplus']);
});

$app->get('/pymesplus/xdoc', function ($request, $response, $args) {
    return $this->view->render($response, 'pymesplusxdoc.twig', ['bodyClass' => 'pymesplus_xdoc', 'menuActive' => 'pymesplus']);
});

$app->get('/financieras/cmb', function ($request, $response, $args) {
    return $this->view->render($response, 'financierascmb.twig', ['bodyClass' => 'financieras_cmb', 'menuActive' => 'financieras']);
});

$app->get('/financieras/bdm', function ($request, $response, $args) {
    return $this->view->render($response, 'financierasbm.twig', ['bodyClass' => 'financieras_bm', 'menuActive' => 'financieras']);
});

$app->get('/gastronomicas', function($req, $res, $args) {
   return $this->view->render($res, 'gastronomicas.twig', ['menuActive' => 'gastronomicas']);
});

$app->get('/test', function ($request, $response, $args) {
    $mail = new \Nette\Mail\Message();
    $mail->setFrom('lautaro ramos <lautaro@softlord.net>')
        ->addTo('ramos.lauty@gmail.com')
        ->setSubject('test')
        ->setBody("testi test");

    $mailer = new Nette\Mail\SmtpMailer([
        'host' => 'smtp.gmail.com',
        'port'  =>  '587',
        'username' => 'lautaro@softlord.net',
        'password' => 'Madrid2015',
        'secure' => 'tls'
    ]);
    $mailer->send($mail);
});

$app->post('/send', function ($request, $response, $args)
{
    $params = $request->getParsedBody();
    $mensaje = $params['mensaje'] ? $params['mensaje'] : "Solicitud de contacto enviada desde el sitio web de softlord";
    $mensaje .=  "\n";
    $mensaje .= "nombre: {$params['nya']}\n";
    $mensaje .= "email de contacto: {$params['email']}\n";
    if(!is_null($params['empresa'])) {
        $mensaje .= "empresa: {$params['empresa']}\n";
    }
    $mail = new \Nette\Mail\Message();
    $mail->setFrom('lautaro ramos <lautaro@softlord.net>')
        ->addTo('lautaro@softlord.net')
        ->setSubject("Solicitud de contacto desde softlord.net")
        ->setBody($mensaje);

    $mailer = new Nette\Mail\SmtpMailer([
        'host' => 'smtp.gmail.com',
        'port'  =>  '587',
        'username' => 'lautaro@softlord.net',
        'password' => 'Madrid2015',
        'secure' => 'tls'
    ]);
    $mailer->send($mail);
});

